package com.example.demo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;


@Entity
@Data
@Table(name="Compras")
public class Producto {
    
	
	@Valid
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotNull
	@Size(max=40)
	private String nombre;
	
	@NotNull
	@Column(name="id_categoria")
	private  Long idCategoria;
	
	@NotNull
	@Column(name="codigo_barras")
	private String codigoBarras;
	
	@NotNull
	@Digits(integer=14, fraction=2)
	@Column(name="precio_venta")
	private Float precio;
	
	@NotNull
	private Integer cantidad;
	
	@NotNull
	private Boolean estado;
	
}